#include <SFML/Graphics.hpp>

#include <iostream>
#include <fstream>
#include <string>

#include "player.hpp"
#include "mobBasique.hpp"
#include "checkpoint.hpp"

#include <sstream>

#define WIN_HEIGHT 600

using namespace sf;
float dt;

int main()
{
    Texture jboyrunTexture;
    if(!jboyrunTexture.loadFromFile("sprites/jboyrun.png"))
        printf("Erreur de chargement de l'animation de course du perso");

    Texture mobTexture;
    if(!mobTexture.loadFromFile("sprites/jboyrun.png"))
        printf("Erreur de chargement de l'animation de course du perso");

    Texture checkpointTexture;
    if(!checkpointTexture.loadFromFile("sprites/checkpoint.png"))
        printf("Erreur de chargement de l'animation du checkpoint");

    Texture idleTexture;
    if(!idleTexture.loadFromFile("sprites/jboyIdle.png"))
        printf("Erreur de chargement de l'animation idle du perso");

    Clock clock;
    Time time;

    Anim testAnim; //Spritesheet, puis nombre de frame, puis fps
    testAnim.texture=jboyrunTexture;
    testAnim.nbframes=7;
    testAnim.fps=12;

    Player player=initPlayer(10,10,jboyrunTexture,idleTexture);

    MobDeBase mobs[200];
    int mobCount=0;

    Checkpoint checkpoints[20];
    int checkCount=0;
    Tilemap tilemap;

    Texture tilesetTexture;
    if(!tilesetTexture.loadFromFile("sprites/tileset.png"))
        printf("Erreur de chargement du tileset");
    addTile(tilemap,tilesetTexture,288,8,8,8,13,1);
    addTile(tilemap,tilesetTexture,296,8,8,8,13,1);
    addTile(tilemap,tilesetTexture,304,8,8,8,13,1);
    addTile(tilemap,tilesetTexture,312,8,8,8,13,1);
    addTile(tilemap,tilesetTexture,288,40,8,8,13,1);
    addTile(tilemap,tilesetTexture,296,40,8,8,13,1);
    addTile(tilemap,tilesetTexture,304,40,8,8,13,1);
    addTile(tilemap,tilesetTexture,312,40,8,8,13,1);
    addTile(tilemap,tilesetTexture,184,72,8,8,13,1);
    addTile(tilemap,tilesetTexture,192,72,8,8,13,1);
    addTile(tilemap,tilesetTexture,216,72,8,8,14,0);
    addTile(tilemap,tilesetTexture,152,80,8,8,13,1);
    addTile(tilemap,tilesetTexture,160,80,8,8,13,1);
    addTile(tilemap,tilesetTexture,216,80,8,8,16,0);
    addTile(tilemap,tilesetTexture,280,80,8,8,15,0);
    addTile(tilemap,tilesetTexture,64,88,8,8,13,1);
    addTile(tilemap,tilesetTexture,72,88,8,8,13,1);
    addTile(tilemap,tilesetTexture,80,88,8,8,13,1);
    addTile(tilemap,tilesetTexture,88,88,8,8,13,1);
    addTile(tilemap,tilesetTexture,96,88,8,8,13,1);
    addTile(tilemap,tilesetTexture,104,88,8,8,13,1);
    addTile(tilemap,tilesetTexture,112,88,8,8,13,1);
    addTile(tilemap,tilesetTexture,120,88,8,8,13,1);
    addTile(tilemap,tilesetTexture,128,88,8,8,13,1);
    addTile(tilemap,tilesetTexture,216,88,8,8,16,0);
    addTile(tilemap,tilesetTexture,280,88,8,8,16,0);
    addTile(tilemap,tilesetTexture,200,96,8,8,1,2);
    addTile(tilemap,tilesetTexture,208,96,8,8,2,2);
    addTile(tilemap,tilesetTexture,216,96,8,8,2,2);
    addTile(tilemap,tilesetTexture,224,96,8,8,11,4);
    addTile(tilemap,tilesetTexture,232,96,8,8,11,4);
    addTile(tilemap,tilesetTexture,240,96,8,8,11,4);
    addTile(tilemap,tilesetTexture,248,96,8,8,2,2);
    addTile(tilemap,tilesetTexture,256,96,8,8,2,2);
    addTile(tilemap,tilesetTexture,264,96,8,8,11,4);
    addTile(tilemap,tilesetTexture,272,96,8,8,11,4);
    addTile(tilemap,tilesetTexture,280,96,8,8,11,4);
    addTile(tilemap,tilesetTexture,288,96,8,8,12,3);
    addTile(tilemap,tilesetTexture,296,96,8,8,12,3);
    addTile(tilemap,tilesetTexture,304,96,8,8,12,3);
    addTile(tilemap,tilesetTexture,312,96,8,8,12,3);
    addTile(tilemap,tilesetTexture,320,96,8,8,3,2);
    addTile(tilemap,tilesetTexture,200,104,8,8,4,2);
    addTile(tilemap,tilesetTexture,208,104,8,8,10,2);
    addTile(tilemap,tilesetTexture,216,104,8,8,10,2);
    addTile(tilemap,tilesetTexture,224,104,8,8,10,2);
    addTile(tilemap,tilesetTexture,232,104,8,8,10,2);
    addTile(tilemap,tilesetTexture,240,104,8,8,10,2);
    addTile(tilemap,tilesetTexture,248,104,8,8,10,2);
    addTile(tilemap,tilesetTexture,256,104,8,8,10,2);
    addTile(tilemap,tilesetTexture,264,104,8,8,10,2);
    addTile(tilemap,tilesetTexture,272,104,8,8,10,2);
    addTile(tilemap,tilesetTexture,280,104,8,8,10,2);
    addTile(tilemap,tilesetTexture,288,104,8,8,10,2);
    addTile(tilemap,tilesetTexture,296,104,8,8,10,2);
    addTile(tilemap,tilesetTexture,304,104,8,8,10,2);
    addTile(tilemap,tilesetTexture,312,104,8,8,10,2);
    addTile(tilemap,tilesetTexture,320,104,8,8,5,2);
    addTile(tilemap,tilesetTexture,200,112,8,8,4,2);
    addTile(tilemap,tilesetTexture,208,112,8,8,10,2);
    addTile(tilemap,tilesetTexture,216,112,8,8,10,2);
    addTile(tilemap,tilesetTexture,224,112,8,8,10,2);
    addTile(tilemap,tilesetTexture,232,112,8,8,10,2);
    addTile(tilemap,tilesetTexture,240,112,8,8,10,2);
    addTile(tilemap,tilesetTexture,248,112,8,8,10,2);
    addTile(tilemap,tilesetTexture,256,112,8,8,10,2);
    addTile(tilemap,tilesetTexture,264,112,8,8,10,2);
    addTile(tilemap,tilesetTexture,272,112,8,8,10,2);
    addTile(tilemap,tilesetTexture,280,112,8,8,10,2);
    addTile(tilemap,tilesetTexture,288,112,8,8,10,2);
    addTile(tilemap,tilesetTexture,296,112,8,8,10,2);
    addTile(tilemap,tilesetTexture,304,112,8,8,10,2);
    addTile(tilemap,tilesetTexture,312,112,8,8,10,2);
    addTile(tilemap,tilesetTexture,320,112,8,8,5,2);
    addTile(tilemap,tilesetTexture,200,120,8,8,4,2);
    addTile(tilemap,tilesetTexture,208,120,8,8,10,2);
    addTile(tilemap,tilesetTexture,216,120,8,8,10,2);
    addTile(tilemap,tilesetTexture,224,120,8,8,10,2);
    addTile(tilemap,tilesetTexture,232,120,8,8,10,2);
    addTile(tilemap,tilesetTexture,240,120,8,8,10,2);
    addTile(tilemap,tilesetTexture,248,120,8,8,10,2);
    addTile(tilemap,tilesetTexture,256,120,8,8,10,2);
    addTile(tilemap,tilesetTexture,264,120,8,8,10,2);
    addTile(tilemap,tilesetTexture,272,120,8,8,10,2);
    addTile(tilemap,tilesetTexture,280,120,8,8,10,2);
    addTile(tilemap,tilesetTexture,288,120,8,8,10,2);
    addTile(tilemap,tilesetTexture,296,120,8,8,10,2);
    addTile(tilemap,tilesetTexture,304,120,8,8,10,2);
    addTile(tilemap,tilesetTexture,312,120,8,8,10,2);
    addTile(tilemap,tilesetTexture,320,120,8,8,5,2);
    addTile(tilemap,tilesetTexture,200,128,8,8,8,2);
    addTile(tilemap,tilesetTexture,208,128,8,8,7,2);
    addTile(tilemap,tilesetTexture,216,128,8,8,7,2);
    addTile(tilemap,tilesetTexture,224,128,8,8,7,2);
    addTile(tilemap,tilesetTexture,232,128,8,8,7,2);
    addTile(tilemap,tilesetTexture,240,128,8,8,7,2);
    addTile(tilemap,tilesetTexture,248,128,8,8,7,2);
    addTile(tilemap,tilesetTexture,256,128,8,8,7,2);
    addTile(tilemap,tilesetTexture,264,128,8,8,7,2);
    addTile(tilemap,tilesetTexture,272,128,8,8,7,2);
    addTile(tilemap,tilesetTexture,280,128,8,8,7,2);
    addTile(tilemap,tilesetTexture,288,128,8,8,7,2);
    addTile(tilemap,tilesetTexture,296,128,8,8,7,2);
    addTile(tilemap,tilesetTexture,304,128,8,8,7,2);
    addTile(tilemap,tilesetTexture,312,128,8,8,7,2);
    addTile(tilemap,tilesetTexture,320,128,8,8,6,2);
    addTile(tilemap,tilesetTexture,64,152,8,8,1,2);
    addTile(tilemap,tilesetTexture,72,152,8,8,2,2);
    addTile(tilemap,tilesetTexture,80,152,8,8,3,2);
    addTile(tilemap,tilesetTexture,0,160,8,8,1,2);
    addTile(tilemap,tilesetTexture,8,160,8,8,2,2);
    addTile(tilemap,tilesetTexture,16,160,8,8,2,2);
    addTile(tilemap,tilesetTexture,24,160,8,8,2,2);
    addTile(tilemap,tilesetTexture,32,160,8,8,2,2);
    addTile(tilemap,tilesetTexture,40,160,8,8,2,2);
    addTile(tilemap,tilesetTexture,48,160,8,8,2,2);
    addTile(tilemap,tilesetTexture,56,160,8,8,2,2);
    addTile(tilemap,tilesetTexture,64,160,8,8,10,2);
    addTile(tilemap,tilesetTexture,72,160,8,8,10,2);
    addTile(tilemap,tilesetTexture,80,160,8,8,10,2);
    addTile(tilemap,tilesetTexture,88,160,8,8,2,2);
    addTile(tilemap,tilesetTexture,96,160,8,8,2,2);
    addTile(tilemap,tilesetTexture,104,160,8,8,3,2);
    addTile(tilemap,tilesetTexture,0,168,8,8,8,2);
    addTile(tilemap,tilesetTexture,8,168,8,8,7,2);
    addTile(tilemap,tilesetTexture,16,168,8,8,7,2);
    addTile(tilemap,tilesetTexture,24,168,8,8,7,2);
    addTile(tilemap,tilesetTexture,32,168,8,8,7,2);
    addTile(tilemap,tilesetTexture,40,168,8,8,7,2);
    addTile(tilemap,tilesetTexture,48,168,8,8,7,2);
    addTile(tilemap,tilesetTexture,56,168,8,8,7,2);
    addTile(tilemap,tilesetTexture,64,168,8,8,7,2);
    addTile(tilemap,tilesetTexture,72,168,8,8,7,2);
    addTile(tilemap,tilesetTexture,80,168,8,8,7,2);
    addTile(tilemap,tilesetTexture,88,168,8,8,7,2);
    addTile(tilemap,tilesetTexture,96,168,8,8,7,2);
    addTile(tilemap,tilesetTexture,104,168,8,8,6,2);
    player.spawnx=15;
    player.spawny=131.25;
    mobs[0]=initMobDeBase(70.25,135.5,mobTexture,false);
    mobCount++;
    mobs[1]=initMobDeBase(81.75,75,mobTexture,false);
    mobCount++;
    mobs[2]=initMobDeBase(123,103.75,mobTexture,false);
    mobCount++;

    checkpoints[0]=initCheckpoint(200,75,checkpointTexture);
    checkCount++;


    addTile(tilemap,tilesetTexture,118.25,121.25,8,8,10,2);
    addTile(tilemap,tilesetTexture,100.75,141,8,8,10,2);
    addTile(tilemap,tilesetTexture,101,100,8,8,10,2);

    player.sprite.setPosition(player.spawnx,player.spawny);

    RenderWindow app(sf::VideoMode(800, 600), "SFML window");
    while (app.isOpen())
    {
        time=clock.restart();
        dt=time.asSeconds();
        //printf("%f\n",dt);
        // Process events
        Event event;
        while (app.pollEvent(event))
        {
            // Close window : exit
            if (event.type == Event::Closed)
                app.close();
        }

        // Clear screen
        app.clear();

        drawMap(tilemap,app);

        updateCheckpoints(checkpoints,checkCount,player,dt);
        drawCheckpoints(checkpoints,checkCount,app);

        updatePlayer(player,tilemap,dt);
        drawPlayer(player,app);

        //updateMobDeBase(mobs[0],tilemap,mobs,mobNumber,player,dt);
        //drawMobDeBase(mobs[0],app);
        updateMobs(mobs,mobCount,tilemap,player,dt);
        drawMobs(mobs,mobCount,app);

        // Update the window
        app.display();
    }
    return 0;
}
