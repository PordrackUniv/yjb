#include <SFML/Graphics.hpp>

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>

#include "player.hpp"

#define GRAVITY 400
#define MAXYSPEED 400
#define JUMPFORCE 200
#define BOUNCEFORCE 400

#define XSPEED 100
#define XACCEL 800
#define JUMPCD  0.5
#define LEFTKEY Keyboard::Left
#define RIGHTKEY Keyboard::Right
#define JUMPKEY Keyboard::Space

#define WIN_HEIGHT 600

using namespace sf;

Player initPlayer(int spawnx,int spawny,Texture &jboyrunTexture,Texture &idleTexture)
{
    Player player;

    // on charge les textures/spritessheet
    player.walkAnim.texture=jboyrunTexture;
    player.walkAnim.nbframes=7;
    player.walkAnim.fps=12;
    player.idleAnim.texture=idleTexture;
    player.idleAnim.nbframes=2;
    player.idleAnim.fps=2;

    //Puis, on charge le reste
    player.spawnx=spawnx;
    player.spawny=spawny;
    player.sprite.setOrigin(player.offsetx+player.width/2,player.offsety);
    player.cAnim=&player.idleAnim;
    player.sprite.setTexture(jboyrunTexture);

    return player;
}

void updatePlayer(Player &player,Tilemap tilemap,float dt)
{
    //On recuper d'abord sa hitbox, pour en faire un rectangle
    float playerx=player.sprite.getPosition().x-0.5*player.width;
    float playery=player.sprite.getPosition().y;

    FloatRect hitboxPlayer(playerx,playery,player.width,player.height);

    //On gere les sauts
    player.jumpcd-=dt; //Cooldown, pour eviter qu'il crame tous les sauts d'un coup
    if(Keyboard::isKeyPressed(JUMPKEY) && player.jumpcd<=0 && player.jumpleft>0)
    {
        player.jumpcd=JUMPCD;
        player.jumpleft--;
        player.yspeed=-JUMPFORCE;
        player.sprite.move(0,-1);
        printf("player.jumpleft=%i",player.jumpleft);
        player.jumpleft--;
        player.yspeed=-JUMPFORCE;
    }


    //On regarde alors toutes les tiles, voir si le joueur a un mur a gauche, un mur a droite, un sol sous ses pieds, un plafond...
    bool onGround=false;
    bool leftWall=false;
    bool rightWall=false;
    bool touchCeil=false;
    /*if(player.jumpcd>0)
        printf("%f\n",player.yspeed);*/

    for(int i=0;i<tilemap.tileNumber;i++)
    {
        int tilex=tilemap.tiles[i].sprite.getPosition().x;
        int tiley=tilemap.tiles[i].sprite.getPosition().y;
        Tile tile=tilemap.tiles[i];

        if(tile.collision>0 && tile.sprite.getGlobalBounds().intersects(FloatRect(playerx,playery+player.height,player.width,0.1))) //On regarde dans un petit rectangle sous les pieds si le sol est là
        {
            onGround=true;
            if(playery+player.height>tiley){
                player.sprite.setPosition(player.sprite.getPosition().x,tiley-player.height); //On le remonte hors du sol si il est bloqué dedans
            }

            if(tile.collision==3) //Plateforme rebondissante
            {
                onGround=false;
                player.yspeed=-BOUNCEFORCE;
            }

            if(tile.collision==4) //Plateforme tueuse
            {
                killPlayer(player);
            }
        }

        if(tile.collision>1 && tile.sprite.getGlobalBounds().intersects(FloatRect(playerx+player.width,playery,0.1,player.height))) //A droite
        {
            rightWall=true;
        }

        if(tile.collision>1 && tile.sprite.getGlobalBounds().intersects(FloatRect(playerx-0.1,playery,0.1,player.height-1))) //A droite
        {
            leftWall=true;
        }

        if(tile.collision>1 && tile.sprite.getGlobalBounds().intersects(FloatRect(playerx,playery-0.1,player.width,player.height-1)))
        {
            touchCeil=true;
            player.sprite.setPosition(player.sprite.getPosition().x,tiley-player.height); //On le remonte hors du sol si il est bloqué dedans
        }

        if(tile.collision>0 && tile.sprite.getGlobalBounds().intersects(FloatRect(playerx+player.width,playery,0.1,player.height))) //A droite
        {
            rightWall=true;
        }

        if(tile.collision>0 && tile.sprite.getGlobalBounds().intersects(FloatRect(playerx-0.1,playery,0.1,player.height-1))) //A droite
        {
            leftWall=true;
        }
    }

    //Modification des vitesses désirés et animations en fonction des touches pressés
    if(Keyboard::isKeyPressed(LEFTKEY)){
        player.dxspeed=-XSPEED;
        player.cAnim=&player.walkAnim;
        if(player.dir==1){
            player.sprite.scale(-1.f,1.f);
            player.dir=-1;
        }
    }
    else if(Keyboard::isKeyPressed(RIGHTKEY)){
        player.dxspeed=XSPEED;
        player.cAnim=&player.walkAnim;
        if(player.dir==-1){
            player.sprite.scale(-1.f,1.f);
            player.dir=1;
        }
    }
    else{
        player.cAnim=&player.idleAnim;
        player.dxspeed=0;
    }

    //On rapproche la vitesse de la vitesse desirée
    if(player.xspeed<player.dxspeed){
        player.xspeed+=XACCEL*dt;
        if(player.xspeed>player.dxspeed) //Si il est desormais "de l'autre coté" car il était tres proche, on le met a la vitesse desirée exact
            player.xspeed=player.dxspeed;
    }
    else if(player.xspeed>player.dxspeed){
        player.xspeed-=XACCEL*dt;
        if(player.xspeed<player.dxspeed) //Si il est desormais "de l'autre coté" car il était tres proche, on le met a la vitesse desirée exact
            player.xspeed=player.dxspeed;
    }

    if(player.yspeed<0 && touchCeil)
        player.yspeed=0;

    //Si il court contre un mur, on annule
    if((player.xspeed>0 && rightWall)||(player.xspeed<0 && leftWall)){
            player.xspeed=0;
    }

    //printf("player.jumpleft=%i,player.jumpmax=%i\n",player.jumpleft,player.jumpmax);
    //Modification des vitesse en fonction de la gravité/des vitesses désirés
    //Si il est pas sur le sol, on le fait tomber
    if(!onGround)
    {
        //On modifie l'animation ici j'imagine
        if(player.yspeed<MAXYSPEED)
            player.yspeed+=dt*GRAVITY;

        if(player.sprite.getPosition().y>WIN_HEIGHT)
            killPlayer(player);
    }
    else if(player.yspeed>0) //Si il vient d'atterir
    {
        player.jumpleft=player.jumpmax; //On recharge ses sauts
        player.yspeed=0;
    }

    //On update la position en fonction des vitesses
    player.sprite.move(dt*player.xspeed,dt*player.yspeed);

    //Animation
    animSprite(player.sprite,*player.cAnim,dt);
}

void drawPlayer(Player player,RenderWindow &window)
{
    /*float playerx=player.sprite.getPosition().x-0.5*player.width;
    float playery=player.sprite.getPosition().y;
    RectangleShape rect(Vector2f(player.width,player.height));
    rect.setPosition(playerx,playery);
    window.draw(rect);*/

    window.draw(player.sprite);
}

void killPlayer(Player &player){
    player.xspeed=0;
    player.yspeed=0;
    player.sprite.setPosition(player.spawnx,player.spawny);
}

