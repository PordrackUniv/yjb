#include <SFML/Graphics.hpp>
#include <time.h>
#include "sidekick.h"
#include "sauvegarde.hpp"
#include "anim.hpp"
#include <c:/sfml/include/SFML/Audio.hpp>
#include <string>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#define LONG_JB 350
#define LARG_JB 800
#define POS_X_JB 10
#define POS_Y_JB 100

int main()
{
    bool reset=false;
    int niv1=3, niv2=1, niv3=1, niv4=1, niv5=1, niv6=1, bestniv1=1, bestniv2=1, bestniv3=1, bestniv4=1, bestniv5=1, bestniv6=1,
    item_n1=0, item_n2=1, item_n3=0, item_n4=0, item_n5=0, item_n6=0, bestitem_n1=0, bestitem_n2=0, bestitem_n3=0, bestitem_n4=0,
    bestitem_n5=0, bestitem_n6=0, scalable=0;
    float scalex=9.6, scaley=12.37, dt;
//
//Cr�ation de la fenetre
//
    sf::VideoMode screen_size = sf::VideoMode::getDesktopMode();
    sf::RenderWindow window_jb(sf::VideoMode(screen_size.width*0.2, screen_size.height*0.7), "Jack le Jukebox");
    window_jb.setPosition(sf::Vector2i(POS_X_JB, POS_Y_JB));
    window_jb.setFramerateLimit(60);

    printf("%i x %i\n", window_jb.getSize().x, window_jb.getSize().y);
//
//Image
//
    sf::Image icon;
    if (!icon.loadFromFile("icon.png"))
        return EXIT_FAILURE;
    window_jb.setIcon(icon.getSize().x, icon.getSize().y, icon.getPixelsPtr());

    sf::Texture jb_idle;
    if (!jb_idle.loadFromFile("./img/jakebox.png"))
        return EXIT_FAILURE;

    sf::Texture jb_talk;
    if (!jb_talk.loadFromFile("./img/jakeboxTalk.png"))
        return EXIT_FAILURE;

    Clock clock;
    Time time;
//
// Animation du jukebox
//
    Anim animidle;
    animidle.texture=jb_idle;
    animidle.nbframes=2;
    animidle.fps=2;

    sf::Sprite animjb;
    animjb.setScale(scalex, scaley);

    Anim animtalk;
    animtalk.texture=jb_talk;
    animtalk.nbframes=4;
    animtalk.fps=4;

    Anim *cAnim;
    cAnim=&animidle;

    sf::Texture bouton1;
    bouton1.loadFromFile("./img/sprite_button0.png");
    sf::Sprite jb_bt1(bouton1);
    jb_bt1.setScale(scalex, scaley);
    jb_bt1.setPosition(86,406);

    //initialisation son
    sf::SoundBuffer plouf;
    if (!plouf.loadFromFile("plouf.wav"))
        return -1;

    sf::SoundBuffer yeha;
    if (!yeha.loadFromFile("son1.wav"))
        return -1;

    sf::Sound noye;
    noye.setBuffer(plouf);

    sf::Sound yay;
    yay.setBuffer(yeha);
//
//Lecture de la sauvegarde
//
    savegame(niv1, niv2, niv3, niv4, niv5, niv6, item_n1, item_n2, item_n3, item_n4, item_n5, item_n6, bestniv1, bestniv2,
             bestniv3, bestniv4, bestniv5, bestniv6, bestitem_n1, bestitem_n2, bestitem_n3, bestitem_n4, bestitem_n5, bestitem_n6, reset);

    printf("%i\n", bestniv1);
    printf("%i\n", bestniv2);
    printf("%i\n", bestniv3);
    printf("%i\n", bestniv4);
    printf("%i\n", bestniv5);
    printf("%i\n", bestniv6);
    printf("%i\n", bestitem_n1);
    printf("%i\n", bestitem_n2);
    printf("%i\n", bestitem_n3);
    printf("%i\n", bestitem_n4);
    printf("%i\n", bestitem_n5);
    printf("%i\n", bestitem_n6);

    if(bestniv1==3){
        cAnim=&animtalk;
    }
/*
    switch (bestlvl){
        case 1:
            printf("Niveau 1\n");
            break;
        case 2:
            printf("Niveau 2\n");
            break;
        case 3:
            printf("Niveau 3\n");
            break;
        case 4:
            printf("Niveau 4\n");
            yay.play();
            break;
        case 5:
            printf("Niveau 5\n");
            break;
    }
*/
	// Start the game loop
    while (window_jb.isOpen())
    {
        time=clock.restart();
        dt=time.asSeconds();
        // Process events
        sf::Event event;
        while (window_jb.pollEvent(event))
        {
            // Close window --> exit
            if (event.type == sf::Event::Closed)
                window_jb.close();

            if (jb_bt1.getGlobalBounds().contains(window_jb.mapPixelToCoords(sf::Mouse::getPosition(window_jb))))
            {
                if (scalable==0)
                {
                    jb_bt1.scale(1.05,1.05);
                    scalable=10;
                }
                if(sf::Mouse::isButtonPressed(sf::Mouse::Left))
                {
                    jb_bt1.setScale(scalex, scaley);
                    printf("bouton appuy�\n");
                }
            }
        }

        // Clear screen
        window_jb.clear();

        // Draw the sprite
        animSprite(animjb, *cAnim, dt);
        window_jb.draw(animjb);
        window_jb.draw(jb_bt1);
        // Update the window
        window_jb.display();
    }

    return EXIT_SUCCESS;
}
