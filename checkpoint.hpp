#ifndef CHECKPOINT_HPP_INCLUDED
#define CHECKPOINT_HPP_INCLUDED
#include <SFML/Graphics.hpp>
#include "anim.hpp"
#include "player.hpp"

using namespace sf;

typedef struct{
    Anim anim;
    Sprite sprite;
    bool isOn;
}Checkpoint;

Checkpoint initCheckpoint(int x,int y,Texture &texture);
void updateCheckpoint(Checkpoint &checkpoint, Player &player,float dt);
void drawCheckpoint(Checkpoint checkpoint,RenderWindow &window);
void updateCheckpoints(Checkpoint *checkpoints,int checkCount,Player &player,float dt);
void drawCheckpoints(Checkpoint *checkpoints,int checkCount,RenderWindow &window);

#endif
