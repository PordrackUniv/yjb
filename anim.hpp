#ifndef ANIM_HPP_INCLUDED
#define ANIM_HPP_INCLUDED

#include <SFML/Graphics.hpp>

using namespace sf;

typedef struct{
    Texture texture;
    int nbframes;
    //int framewidth;
    //int frameheight;
    int fps;
    int cframe=0;
    float crawframe=0;
}Anim;

void animSprite(Sprite &sprite,Anim &anim,float dt);

#endif

