#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include "sauvegarde.hpp"

void savegame(int niv1, int niv2, int niv3, int niv4, int niv5, int niv6, int itemniv1, int itemniv2, int itemniv3, int itemniv4,
              int itemniv5, int itemniv6, int& bestniv1, int& bestniv2, int& bestniv3, int& bestniv4, int& bestniv5, int& bestniv6,
              int& bestitemniv1, int& bestitemniv2, int& bestitemniv3, int& bestitemniv4, int& bestitemniv5, int& bestitemniv6, bool& reset){
//
//Chargement des donn�es
//
    std::ifstream saveFileInput ("profile.txt");
    std::string content1;
    std::string content2;
    std::string content3;
    std::string content4;
    std::string content5;
    std::string content6;
    std::string content7;
    std::string content8;
    std::string content9;
    std::string content10;
    std::string content11;
    std::string content12;
    if (saveFileInput.is_open())
    {
        std::getline(saveFileInput,content1, '\n');
        std::stringstream intniv1(content1);
        intniv1>>bestniv1;
        std::getline(saveFileInput,content2, '\n');
        std::stringstream intniv2(content2);
        intniv2>>bestniv2;
        std::getline(saveFileInput,content3, '\n');
        std::stringstream intniv3(content3);
        intniv3>>bestniv3;
        std::getline(saveFileInput,content4, '\n');
        std::stringstream intniv4(content4);
        intniv4>>bestniv4;
        std::getline(saveFileInput,content5, '\n');
        std::stringstream intniv5(content5);
        intniv5>>bestniv5;
        std::getline(saveFileInput,content6, '\n');
        std::stringstream intniv6(content6);
        intniv6>>bestniv6;
        std::getline(saveFileInput,content7, '\n');
        std::stringstream intitemniv1(content7);
        intitemniv1>>bestitemniv1;
        std::getline(saveFileInput,content8, '\n');
        std::stringstream intitemniv2(content8);
        intitemniv2>>bestitemniv2;
        std::getline(saveFileInput,content9, '\n');
        std::stringstream intitemniv3(content9);
        intitemniv3>>bestitemniv3;
        std::getline(saveFileInput,content10, '\n');
        std::stringstream intitemniv4(content10);
        intitemniv4>>bestitemniv4;
        std::getline(saveFileInput,content11, '\n');
        std::stringstream intitemniv5(content11);
        intitemniv5>>bestitemniv5;
        std::getline(saveFileInput,content12, '\n');
        std::stringstream intitemniv6(content12);
        intitemniv6>>bestitemniv6;
        saveFileInput.close();
    }
//
//Sauvegarde les donn�es
//
    if(niv1>bestniv1)
        bestniv1=niv1;
    if(niv2>bestniv2)
        bestniv2=niv2;
    if(niv3>bestniv3)
        bestniv3=niv3;
    if(niv4>bestniv4)
        bestniv4=niv4;
    if(niv5>bestniv5)
        bestniv5=niv5;
    if(niv6>bestniv6)
        bestniv6=niv6;

    if(itemniv1>bestitemniv1)
        bestitemniv1=itemniv1;
    if(itemniv2>bestitemniv2)
        bestitemniv2=itemniv2;
    if(itemniv3>bestitemniv3)
        bestitemniv3=itemniv3;
    if(itemniv4>bestitemniv4)
        bestitemniv4=itemniv4;
    if(itemniv5>bestitemniv5)
        bestitemniv5=itemniv5;
    if(itemniv6>bestitemniv6)
        bestitemniv6=itemniv6;

    std::ofstream saveFileOutput ("profile.txt");
    if (saveFileOutput.is_open())
    {
        if(reset==true){
                bestniv1=2, bestniv2=1, bestniv3=1, bestniv4=1, bestniv5=1, bestniv6=1, bestitemniv1=0, bestitemniv2=0, bestitemniv3=0, bestitemniv4=0,
                bestitemniv5=0, bestitemniv6=0;
        }
        saveFileOutput << bestniv1 << std::endl << bestniv2 << std::endl << bestniv3 << std::endl << bestniv4 << std::endl <<
        bestniv5 << std::endl << bestniv6 << std::endl << bestitemniv1 << std::endl << bestitemniv2 << std::endl << bestitemniv3
        << std::endl << bestitemniv4 << std::endl << bestitemniv5 << std::endl << bestitemniv6;
    }

}
