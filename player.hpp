#ifndef PLAYER_HPP_INCLUDED
#define PLAYER_HPP_INCLUDED


#include <SFML/Graphics.hpp>
#include "anim.hpp"
#include "tile.hpp"

using namespace sf;

typedef struct{
    Anim walkAnim,idleAnim;
    Anim* cAnim;
    Sprite sprite;
    float xspeed=0;
    float yspeed=0;
    int dxspeed=0;
    int jumpmax=2;
    int jumpleft=0;
    float jumpcd=0;
    float spawnx,spawny;
    int width=15; //Dimension de la hitbox
    int height=18;
    int offsetx=2; //Ecart entre le coin du "sprite", et de la hitbox
    int offsety=2;
    int dir=1; //Le sens dans le quel il est tourne, 1=droite, 0=gauche
}Player;

Player initPlayer(int spawnx,int spawny,Texture &jboyrunTexture,Texture &idleTexture);
void updatePlayer(Player &player,Tilemap tilemap,float dt);
void drawPlayer(Player player,RenderWindow &window);
void killPlayer(Player &player);

#endif


