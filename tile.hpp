#ifndef TILE_HPP_INCLUDED
#define TILE_HPP_INCLUDED


#include <SFML/Graphics.hpp>
#define MAXTILES 200

using namespace sf;

typedef struct{
    Sprite sprite;
    int collision;
    //int damage;
}Tile;

typedef struct{
    int tileNumber=0;
    Tile tiles[MAXTILES]={};
}Tilemap;

void addTile(Tilemap &tilemap,Texture &texture,int x,int y,int width,int height,int tileNumber,int collision);//,int damage);
void drawMap(Tilemap tilemap,RenderWindow &window);

#endif

