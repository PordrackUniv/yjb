#include <SFML/Graphics.hpp>
using namespace sf;

void eventsPermanents(RenderWindow &thatWindow, Event &event, int &whatToDraw);

void eventsMenuPrincipal(RenderWindow &thatWindow, Event &event, int &whatToDraw, int &scalable, Sprite &bouton_jouer, Sprite &place_Holder1, Sprite &place_Holder2);

void eventsSelecNiveaux(RenderWindow &thatWindow, Event &event, int &whatToDraw,int &scalable, Sprite &bouton_Niveau);

void gererEvents(Event &event, Sprite &bouton_jouer, Sprite &place_Holder1, Sprite &place_Holder2, Sprite &bouton_Niveau, int &whatToDraw, int &scalable, RenderWindow &thatWindow);

