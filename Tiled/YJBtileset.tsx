<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.4" tiledversion="1.4.2" name="YJBtileset" tilewidth="8" tileheight="8" tilecount="18" columns="18" objectalignment="topleft">
 <image source="../sprites/tileset.png" width="144" height="8"/>
 <tile id="0">
  <properties>
   <property name="coll" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="1">
  <properties>
   <property name="coll" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="2">
  <properties>
   <property name="coll" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="3">
  <properties>
   <property name="coll" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="4">
  <properties>
   <property name="coll" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="5">
  <properties>
   <property name="coll" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="6">
  <properties>
   <property name="coll" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="7">
  <properties>
   <property name="coll" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="8">
  <properties>
   <property name="coll" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="9">
  <properties>
   <property name="coll" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="10">
  <properties>
   <property name="coll" type="int" value="4"/>
  </properties>
 </tile>
 <tile id="11">
  <properties>
   <property name="coll" type="int" value="3"/>
  </properties>
 </tile>
 <tile id="12">
  <properties>
   <property name="coll" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="13">
  <properties>
   <property name="coll" type="int" value="0"/>
  </properties>
 </tile>
 <tile id="14">
  <properties>
   <property name="coll" type="int" value="0"/>
  </properties>
 </tile>
 <tile id="15">
  <properties>
   <property name="coll" type="int" value="0"/>
  </properties>
 </tile>
 <tile id="16">
  <properties>
   <property name="coll" type="int" value="0"/>
  </properties>
 </tile>
 <tile id="17">
  <properties>
   <property name="coll" type="int" value="0"/>
  </properties>
 </tile>
</tileset>
