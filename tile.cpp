#include <SFML/Graphics.hpp>

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>

#include "tile.hpp"

using namespace sf;

void addTile(Tilemap &tilemap,Texture &texture,int x,int y,int width,int height,int tileNumber,int collision)//,int damage)
{
    Sprite tileSprite;
    tileSprite.setTexture(texture);
    tileSprite.setPosition(x,y);
    tileNumber--; //Pour compenser la tile 0, enfin, le fait que le code commence a compter a 0, mais pas Piskel/tiled
    tileSprite.setTextureRect(IntRect(tileNumber*width,0,width,height));

    Tile newTile={tileSprite,collision};

    tilemap.tiles[tilemap.tileNumber]=newTile;
    tilemap.tileNumber++;
}

void drawMap(Tilemap tilemap,RenderWindow &window)
{
    int i;

    /*Sprite test;
    Texture tilesetTexture;
    if(!tilesetTexture.loadFromFile("sprites/tileset.png"))
        printf("Erreur de chargement de l'animation de course du perso");

    test.setTexture(tilesetTexture);
    test.setPosition(20,20);*/

    for(i=0;i<tilemap.tileNumber;i++){
        window.draw(tilemap.tiles[i].sprite);
        //window.draw(test);
        //printf("Sprite %i\n",i);
    }
}
