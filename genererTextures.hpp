#ifndef GENERERTEXTURES_HPP_INCLUDED
#define GENERERTEXTURES_HPP_INCLUDED

#include <SFML/Graphics.hpp>
#include "player.hpp"
#include "tile.hpp"
using namespace sf;

Texture genTexture(char char1[]);

void gererDraw(RenderWindow &thatWindow,int &whatToDraw,Sprite &menu_Option,Sprite &menu,Sprite &bouton_jouer,Sprite &place_Holder1,Sprite &place_Holder2,Sprite &menu_Niveau,Sprite &bouton_Niveau,Sprite &sprite, Tilemap tilemap, Player player);

#endif // GENERERTEXTURES_HPP_INCLUDED
