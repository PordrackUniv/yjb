#include <SFML/Graphics.hpp>

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>

#include "anim.hpp"

using namespace sf;

void animSprite(Sprite &sprite,Anim &anim,float dt)
{

    int framewidth=anim.texture.getSize().x/anim.nbframes;//anim.framewidth;
    anim.crawframe+=anim.fps*dt;

    if(anim.crawframe>anim.nbframes)
        anim.crawframe=0;

    anim.cframe=anim.crawframe;
    //printf("anim.cframe=%i\n",anim.cframe);

    //if(sprite.getTexture()!=anim.texture)
    sprite.setTexture(anim.texture);

    sprite.setTextureRect(IntRect(anim.cframe*framewidth,0,framewidth,anim.texture.getSize().y));//framewidth,anim.frameheight));
}
