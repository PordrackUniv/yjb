#include <SFML/Graphics.hpp>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include "checkpoint.hpp"



using namespace sf;
Checkpoint initCheckpoint(int x,int y,Texture &texture)
{
    Checkpoint checkpoint;

    checkpoint.anim.texture=texture;
    checkpoint.anim.nbframes=7;
    checkpoint.anim.fps=6;

    //checkpoint.cAnim=&checkpoint.idleAnim;
    checkpoint.sprite.setTexture(texture);
    checkpoint.sprite.setPosition(x, y);

    //On initialise son animation, mais elle sera fig� tant qu'il n'est pas activ�
    animSprite(checkpoint.sprite,checkpoint.anim,0);

    return checkpoint;
}

void updateCheckpoint(Checkpoint &checkpoint, Player &player,float dt)
{
    //On recuper d'abord sa hitbox, pour en faire un rectangle
    FloatRect hitboxCheckpoint=checkpoint.sprite.getGlobalBounds();

    if(!checkpoint.isOn)
    {
        //On va d'abord regarder la hitbox du joueur, pour save si il y'a contact
        float playerx=player.sprite.getPosition().x-0.5*player.width;
        float playery=player.sprite.getPosition().y;
        FloatRect hitboxPlayer(playerx,playery,player.width,player.height);
        if(hitboxPlayer.intersects(hitboxCheckpoint)){ //On tue le joueur
            player.spawnx=checkpoint.sprite.getPosition().x;
            player.spawny=checkpoint.sprite.getPosition().y;
            checkpoint.isOn=true;
        }
        animSprite(checkpoint.sprite,checkpoint.anim,0);
    }
    else if(player.spawnx==checkpoint.sprite.getPosition().x) //Animation
        animSprite(checkpoint.sprite,checkpoint.anim,dt);
}

void drawCheckpoint(Checkpoint checkpoint,RenderWindow &window)
{
    window.draw(checkpoint.sprite);
}

void updateCheckpoints(Checkpoint *checkpoints,int checkCount,Player &player,float dt){
    for(int i=0;i<checkCount;i++){
        updateCheckpoint(checkpoints[i],player,dt);
    }
}

void drawCheckpoints(Checkpoint *checkpoints,int checkCount,RenderWindow &window){
    for(int i=0;i<checkCount;i++){
        drawCheckpoint(checkpoints[i],window);
    }
}

