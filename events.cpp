#include "events.hpp"

void eventsPermanents(RenderWindow &thatWindow, Event &event, int &whatToDraw)
{
    if (event.type == Event::Closed || (event.type == Event::KeyPressed && event.key.code == Keyboard::Escape))
    {
        thatWindow.close();
    }
    else if (event.type == Event::KeyPressed && event.key.code == Keyboard::Down)
    {
        ++whatToDraw;
    }
    else if (event.type == Event::KeyPressed && event.key.code == Keyboard::Up)
    {
        --whatToDraw;
    }
}

void eventsMenuPrincipal(RenderWindow &thatWindow, Event &event, int &whatToDraw, int &scalable, Sprite &bouton_jouer, Sprite &place_Holder1, Sprite &place_Holder2)
{
    if (bouton_jouer.getGlobalBounds().contains(thatWindow.mapPixelToCoords(Mouse::getPosition(thatWindow))))
    {
        if (scalable==0)
        {
            printf("Hoi\n");
            bouton_jouer.scale(1.05,1.05);
            scalable=1;
        }
        if(Mouse::isButtonPressed(Mouse::Left))
        {
            ++whatToDraw;
            printf("Hoi %i\n",whatToDraw);
        }
    }

    else if (place_Holder1.getGlobalBounds().contains(thatWindow.mapPixelToCoords(Mouse::getPosition(thatWindow))))
    {
        if (scalable==0)
        {
            place_Holder1.scale(1.05,1.05);
            scalable=2;
        }
        if(Mouse::isButtonPressed(Mouse::Left))
        {
            --whatToDraw;
            printf("Hoi %i\n",whatToDraw);
        }
    }

    else if (place_Holder2.getGlobalBounds().contains(thatWindow.mapPixelToCoords(Mouse::getPosition(thatWindow))))
    {
        if (scalable==0)
        {
            place_Holder2.scale(1.05,1.05);
            scalable=3;
        }
        if(Mouse::isButtonPressed(Mouse::Left))
        {
            whatToDraw=-10;
            printf("Hoi %i\n",whatToDraw);
        }
    }
    else switch (scalable)
    {
        case 1 :
            printf("Haaai\n");
            bouton_jouer.scale(0.953,0.953);
            scalable=0;
            break;
        case 2 :
            place_Holder1.scale(0.953,0.953);
            scalable=0;
            break;
        case 3 :
            place_Holder2.scale(0.953,0.953);
            scalable=0;
        case 0 :
            break;
        default :
            printf("Hoi %i\n",whatToDraw);
            break;
    }
}

void eventsSelecNiveaux(RenderWindow &thatWindow, Event &event, int &whatToDraw,int &scalable, Sprite &bouton_Niveau)
{
    if (bouton_Niveau.getGlobalBounds().contains(thatWindow.mapPixelToCoords(Mouse::getPosition(thatWindow))))
    {
        if (scalable==0)
        {
            bouton_Niveau.scale(1.05,1.05);
            scalable=10;
        }
        if(Mouse::isButtonPressed(Mouse::Left))
        {
            ++whatToDraw;
            printf("Hoi %i\n",whatToDraw);
        }
    }
}

void gererEvents(Event &event, Sprite &bouton_jouer, Sprite &place_Holder1, Sprite &place_Holder2, Sprite &bouton_Niveau, int &whatToDraw, int &scalable, RenderWindow &thatWindow)
{
    //printf("wtd : %d\n", whatToDraw);

    eventsPermanents(thatWindow, event, whatToDraw);

    switch (whatToDraw)
    {
        case 0 : //Menu Principal
            eventsMenuPrincipal(thatWindow, event, whatToDraw, scalable, bouton_jouer, place_Holder1, place_Holder2);
            break;

        case 1 :
            eventsSelecNiveaux(thatWindow, event, whatToDraw, scalable, bouton_Niveau);
            break;
    }
}

