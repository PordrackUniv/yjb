#include <SFML/Graphics.hpp>
//#include <iostream>
//#include <fstream>
//#include <string>
//#include <sstream>
#include "player.hpp"
#include "events.hpp"
#include "sauvegarde.hpp"
#include "genererTextures.hpp"

#define DELAY 1
using namespace sf;


/*void delay(int milliseconds)
{
    long pause;
    clock_t now,then;

    pause = milliseconds*(CLOCKS_PER_SEC/1000);
    now = then = clock();
    while( (now-then) < pause )
        now = clock();
}*/

int main()
{
    float dt;
    int whatToDraw=0, scalable=0;
    char char1[15]="Images\\cb.bmp", char2[30]="Images\\Menus\\Menu0.1.jpg",char3[35]="Images\\Boutons\\BouttonJouer.PNG",char4[25]="Images\\PlaceHolder.PNG";
    char char5[35]="Images\\Menus\\menu_Niveaux_0.1.jpg",char6[40]="Images\\Boutons\\bouton_niveau_0.2.png",char7[35]="Images\\Menus\\Menu_Option_0.1.jpg";
    char char8[30]="Images\\Sprites\\tileset.png",char9[35]="Images\\Sprites\\jboyrun.png",char10[35]="Images\\Sprites\\jboyrun.png";
    char char11[30]="Images\\Boutons\\boutonOn.png",char12[35]="Images\\Boutons\\boutonOff.png",char13[35]="Images\\Boutons\\niveauBloque.png";
    char char14[40]="Images\\Boutons\\niveauDebloque.png",char15[40]="Images\\Boutons\\niveauNonComplete.png";
    RenderWindow gameWindow(VideoMode(800, 600), "Game");

    Texture texture=genTexture(char1);
    Texture menuTexture=genTexture(char2);
    Texture boutonJouer=genTexture(char3);
    Texture placeHolder=genTexture(char4);
    Texture menuNiveau=genTexture(char5);
    Texture boutonNiveau=genTexture(char6);
    Texture menuOption=genTexture(char7);
    Texture tilesetTexture=genTexture(char8);
    Texture jboyrunTexture=genTexture(char9);
    Texture idleTexture=genTexture(char10);
    Texture boutonOn=genTexture(char11);
    Texture boutonOff=genTexture(char12);
    Texture niveauBloque=genTexture(char13);
    Texture niveauDebloque=genTexture(char14);
    Texture niveauComplete=genTexture(char15);

    Sprite sprite(texture);
    Sprite menu(menuTexture);               menu.scale(0.5,0.5);
    Sprite bouton_jouer(boutonJouer);
    Sprite place_Holder1(placeHolder);      place_Holder1.scale(2,0.6);
    Sprite place_Holder2(placeHolder);      place_Holder2.scale(2,0.6);
    Sprite menu_Niveau(menuNiveau);         menu_Niveau.scale(0.5,0.5);
    Sprite bouton_Niveau(boutonNiveau);     bouton_Niveau.scale(0.3,0.3);
    Sprite menu_Option(menuOption);         menu_Option.scale(0.5,0.5);
    Sprite bouton_On(boutonOn);           //  bouton_On.scale(0.5,0.5);
    Sprite bouton_On2(boutonOn);            bouton_On2.scale(0.5,0.5);
    Sprite bouton_Off(boutonOff);        //   bouton_Off.scale(0.5,0.5);
    Sprite bouton_Off2(boutonOff);          bouton_Off2.scale(0.5,0.5);
    Sprite niveau_Bloque(niveauBloque);     niveau_Bloque.scale(0.5,0.5);
    Sprite niveau_Debloque(niveauDebloque); niveau_Debloque.scale(0.5,0.5);
    Sprite niveau_Complete(niveauComplete); niveau_Complete.scale(0.5,0.5);

    bouton_jouer.setPosition(200,200);
    place_Holder1.setPosition(100,280);
    place_Holder2.setPosition(450,280);
    bouton_Niveau.setPosition(50,175);
    bouton_On.setPosition(600,90);
    bouton_Off.setPosition(600,-90);
    bouton_On2.setPosition(600,130);
    bouton_Off2.setPosition(600,130);


    gameWindow.setFramerateLimit(60);
    Clock clock = Clock();
  //  Clock clock;      Used by Pordrack
    Time time;

    Tilemap tilemap;
    addTile(tilemap,tilesetTexture,0,50,8,8,1,1);
    addTile(tilemap,tilesetTexture,8,50,8,8,2,1);
    addTile(tilemap,tilesetTexture,16,50,8,8,2,1);
    addTile(tilemap,tilesetTexture,24,50,8,8,3,1);
    addTile(tilemap,tilesetTexture,0,58,8,8,8,1);
    addTile(tilemap,tilesetTexture,8,58,8,8,7,1);
    addTile(tilemap,tilesetTexture,16,58,8,8,7,1);
    addTile(tilemap,tilesetTexture,24,58,8,8,6,1);

    addTile(tilemap,tilesetTexture,30+0,30,8,8,1,1);
    addTile(tilemap,tilesetTexture,30+8,30,8,8,2,1);
    addTile(tilemap,tilesetTexture,30+16,30,8,8,2,1);
    addTile(tilemap,tilesetTexture,30+24,30,8,8,3,1);
    addTile(tilemap,tilesetTexture,30+0,38,8,8,8,1);
    addTile(tilemap,tilesetTexture,30+8,38,8,8,7,1);
    addTile(tilemap,tilesetTexture,30+16,38,8,8,7,1);
    addTile(tilemap,tilesetTexture,30+24,38,8,8,6,1);

    Player player=initPlayer(10,10,jboyrunTexture,idleTexture);

    while (1)
    {
        time=clock.restart();
        dt=time.asSeconds();

        Event event;
        while (gameWindow.pollEvent(event))
        {
             gererEvents(event, bouton_jouer, place_Holder1, place_Holder2, bouton_Niveau, bouton_On, bouton_Off, whatToDraw, scalable, gameWindow);
        }
        gameWindow.clear();
        gererDraw(gameWindow,whatToDraw, menu_Option, menu,bouton_jouer,place_Holder1,place_Holder2,menu_Niveau,bouton_Niveau,sprite, bouton_On, bouton_Off, tilemap, player);
        gameWindow.display();

        /*delay(DELAY);
        float currentTime = clock.restart().asSeconds();
        float fps = 1.f / (currentTime);
        printf("%.0f : FPS\n",fps);*/
    }
    return EXIT_SUCCESS;
}
