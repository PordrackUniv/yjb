#include <SFML/Graphics.hpp>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include "mobBasique.hpp"

#define GRAVITY 400
#define MAXYSPEED 400
#define XSPEED 100
#define XACCEL 1000 //Plus c'est �lev�, moins c'est savonette

using namespace sf;

<<<<<<< HEAD
MobDeBase initMobDeBase(int spawnx,int spawny,Texture &mechantRunTexture,bool isAlly)//Texture &mechantIdleTexture,bool isAlly)
{
    MobDeBase mob1;

    mob1.isAlly=isAlly;

=======
MobDeBase initMobDeBase(int spawnx,int spawny,Texture &mechantRunTexture,Texture &mechantIdleTexture)
{
    MobDeBase mob1;

>>>>>>> 85095c74805a671938f4bec922cb1c06523909b6
    // on charge les textures/spritessheet
    mob1.walkAnim.texture=mechantRunTexture;
    mob1.walkAnim.nbframes=7;
    mob1.walkAnim.fps=12;
<<<<<<< HEAD
    /*mob1.idleAnim.texture=mechantIdleTexture;
    mob1.idleAnim.nbframes=2;
    mob1.idleAnim.fps=2;*/
=======
    mob1.idleAnim.texture=mechantIdleTexture;
    mob1.idleAnim.nbframes=2;
    mob1.idleAnim.fps=2;
>>>>>>> 85095c74805a671938f4bec922cb1c06523909b6

    //Puis, on charge le reste
    mob1.spawnx=spawnx;
    mob1.spawny=spawny;
    mob1.sprite.setOrigin(mob1.offsetx+mob1.width/2,mob1.offsety);
<<<<<<< HEAD
    //mob1.cAnim=&mob1.idleAnim;
=======
    mob1.cAnim=&mob1.idleAnim;
>>>>>>> 85095c74805a671938f4bec922cb1c06523909b6
    mob1.sprite.setTexture(mechantRunTexture);
    mob1.sprite.setPosition(mob1.spawnx, mob1.spawny);

    return mob1;
}

<<<<<<< HEAD
void updateMobDeBase(MobDeBase &mob1,Tilemap tilemap,MobDeBase *otherMobs,int mobCount,Player &player,float dt)
=======
void updateMobDeBase(MobDeBase &mob1,Tilemap tilemap,float dt)
>>>>>>> 85095c74805a671938f4bec922cb1c06523909b6
{
    //On recuper d'abord sa hitbox, pour en faire un rectangle
    float mob1x=mob1.sprite.getPosition().x-0.5*mob1.width;
    float mob1y=mob1.sprite.getPosition().y;

    FloatRect hitboxMobDeBase(mob1x,mob1y,mob1.width,mob1.height);

<<<<<<< HEAD
    if(!mob1.isAlly)
    {
        //On va d'abord regarder la hitbox du joueur, pour le tuer si il y'as contact
        float playerx=player.sprite.getPosition().x-0.5*player.width;
        float playery=player.sprite.getPosition().y;
        FloatRect hitboxPlayer(playerx,playery,player.width,player.height);
        if(hitboxPlayer.intersects(hitboxMobDeBase)){ //On tue le joueur
            killPlayer(player);
        }

        for(int i=0;i<mobCount;i++){ //Puis on regarde si un mob gentil nous touche, si oui, suicide
            MobDeBase omob=otherMobs[i];
            if(omob.isAlly){
                float omobx=omob.sprite.getPosition().x-0.5*omob.width;
                float omoby=omob.sprite.getPosition().y;

                FloatRect hitboxOmob(omobx,omoby,omob.width,omob.height);
                if(hitboxMobDeBase.intersects(hitboxOmob)){
                    mob1.sprite.setPosition(20000,20000);
                }
            }
        }
    }

    //On regarde alors toutes les tiles, voir si le mob a un mur a gauche, un mur a droite, un sol sous ses pieds, un plafond...
    bool onGround=false;
    bool leftWall=false;
    bool rightWall=false;
    bool onLedge=true;
    int ledgePointx,ledgePointy;

    ledgePointy=mob1y+mob1.height+1;
    if(mob1.dxspeed<0)
        ledgePointx=mob1x-1;
    else
        ledgePointx=mob1x+mob1.width+1;

=======
    //On regarde alors toutes les tiles, voir si le joueur a un mur a gauche, un mur a droite, un sol sous ses pieds, un plafond...
    bool onGround=false;
    bool leftWall=false;
    bool rightWall=false;
>>>>>>> 85095c74805a671938f4bec922cb1c06523909b6

    for(int i=0;i<tilemap.tileNumber;i++)
    {
        int tilex=tilemap.tiles[i].sprite.getPosition().x;
        int tiley=tilemap.tiles[i].sprite.getPosition().y;
        Tile tile=tilemap.tiles[i];

        if(tile.collision>0 && tile.sprite.getGlobalBounds().intersects(FloatRect(mob1x,mob1y+mob1.height,mob1.width,0.1))) //On regarde dans un petit rectangle sous les pieds si le sol est l�
        {
            onGround=true;
            mob1.sprite.setPosition(mob1.sprite.getPosition().x,tiley-mob1.height); //On le remonte hors du sol si il est bloqu� dedans
        }

<<<<<<< HEAD
        if(tile.collision>1 && tile.sprite.getGlobalBounds().intersects(FloatRect(mob1x+mob1.width,mob1y,0.1,mob1.height))) //A droite
=======
        if(tile.collision>0 && tile.sprite.getGlobalBounds().intersects(FloatRect(mob1x+mob1.width,mob1y,0.1,mob1.height))) //A droite
>>>>>>> 85095c74805a671938f4bec922cb1c06523909b6
        {
            rightWall=true;
        }

<<<<<<< HEAD
        if(tile.collision>1 && tile.sprite.getGlobalBounds().intersects(FloatRect(mob1x-0.1,mob1y,0.1,mob1.height-1))) //A droite
        {
            leftWall=true;
        }


        if(tile.collision>0 && tile.sprite.getGlobalBounds().contains(ledgePointx,ledgePointy))
        {
            onLedge=false;
        }

    }

    if (onGround){
        if (mob1.xspeed>0 && (rightWall || onLedge)){
            mob1.dxspeed=-XSPEED;
            if(mob1.dir==1){
                mob1.sprite.scale(-1.f,1.f);
                mob1.dir=-1;
            }
            //mob1.cAnim=&mob1.walkAnim;
        }
        else if (mob1.xspeed<0 && (leftWall || onLedge)){
            mob1.dxspeed=XSPEED;
            if(mob1.dir==-1){
                mob1.sprite.scale(-1.f,1.f);
                mob1.dir=1;
            }
            //mob1.cAnim=&mob1.walkAnim;
        }
        else if (mob1.xspeed==0){
            mob1.dxspeed=XSPEED;
            if(mob1.dir==-1){
                mob1.sprite.scale(-1.f,1.f);
                mob1.dir=1;
            }
=======
        if(tile.collision>0 && tile.sprite.getGlobalBounds().intersects(FloatRect(mob1x-0.1,mob1y,0.1,mob1.height-1))) //A droite
        {
            leftWall=true;
        }
    }

    if (onGround){
        if (mob1.xspeed>0 && rightWall){
            mob1.dxspeed=-XSPEED;
            mob1.cAnim=&mob1.walkAnim;
        }
        else if (mob1.xspeed<0 && leftWall){
            mob1.dxspeed=XSPEED;
            mob1.cAnim=&mob1.walkAnim;
        }
        else if (mob1.xspeed==0){
            mob1.dxspeed=XSPEED;
>>>>>>> 85095c74805a671938f4bec922cb1c06523909b6
        }
    }

    //On rapproche la vitesse de la vitesse desir�e
    if(mob1.xspeed<mob1.dxspeed){
        mob1.xspeed+=XACCEL*dt;
        if(mob1.xspeed>mob1.dxspeed) //Si il est desormais "de l'autre cot�" car il �tait tres proche, on le met a la vitesse desir�e exact
            mob1.xspeed=mob1.dxspeed;
    }
    else if(mob1.xspeed>mob1.dxspeed){
        mob1.xspeed-=XACCEL*dt;
        if(mob1.xspeed<mob1.dxspeed) //Si il est desormais "de l'autre cot�" car il �tait tres proche, on le met a la vitesse desir�e exact
            mob1.xspeed=mob1.dxspeed;
    }


    //Modification des vitesse en fonction de la gravit�/des vitesses d�sir�s
    //Si il est pas sur le sol, on le fait tomber
    if(!onGround)
    {
        //On modifie l'animation ici j'imagine
        if(mob1.yspeed<MAXYSPEED)
            mob1.yspeed+=dt*GRAVITY;
    }
    else if(mob1.yspeed>0) //Si il vient d'atterir
    {
        mob1.yspeed=0;
    }

    //On update la position en fonction des vitesses
    mob1.sprite.move(dt*mob1.xspeed,dt*mob1.yspeed);

    //Animation
<<<<<<< HEAD
    animSprite(mob1.sprite,mob1.walkAnim,dt);
=======
    animSprite(mob1.sprite,*mob1.cAnim,dt);
>>>>>>> 85095c74805a671938f4bec922cb1c06523909b6
}

void drawMobDeBase(MobDeBase mob1,RenderWindow &window)
{
<<<<<<< HEAD
    /*float mob1x=mob1.sprite.getPosition().x-0.5*mob1.width;
    float mob1y=mob1.sprite.getPosition().y;
    RectangleShape rect(Vector2f(mob1.width,mob1.height));
    rect.setPosition(mob1x,mob1y);*/
=======
    float mob1x=mob1.sprite.getPosition().x-0.5*mob1.width;
    float mob1y=mob1.sprite.getPosition().y;
    RectangleShape rect(Vector2f(mob1.width,mob1.height));
    rect.setPosition(mob1x,mob1y);
>>>>>>> 85095c74805a671938f4bec922cb1c06523909b6
    //window.draw(rect);
    window.draw(mob1.sprite);
}

<<<<<<< HEAD
void updateMobs(MobDeBase *mobs,int mobCount,Tilemap tilemap,Player &player,float dt){
    for(int i=0;i<mobCount;i++){
        updateMobDeBase(mobs[i],tilemap,mobs,mobCount,player,dt);
    }
}

void drawMobs(MobDeBase *mobs,int mobCount,RenderWindow &window){
    for(int i=0;i<mobCount;i++){
        drawMobDeBase(mobs[i],window);
    }
}

/*void addMob(MobDeBase *iArray,int &mobCount,MobDeBase newMob){
    MobDeBase newArray[mobCount+1]; //Creer un nouveau tableau, qui contiendra l'ancien
    for(int i=0;i<mobCount;i++){
        newArray[i]=iArray[i]; //copie 1 a 1 les elements de l'ancien tableau
    }
    newArray[mobCount]=newMob; //Ajoute le nouveau mob a la fin
    mobCount++; //Incremente mobCount
    iArray=newArray;
}*/

=======
>>>>>>> 85095c74805a671938f4bec922cb1c06523909b6

